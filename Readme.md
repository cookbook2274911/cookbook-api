## Getting started

Run the following commands to be able to use the generated artifact.

```bash
gradle build
```

This runs the following tasks:
- generates OpenAPI sources
- copies the generated sources to the `src` folder
- complies the generated sources

Then use this command to publish the artifact to your local maven repository.

```bash
gradle publish
```

The dependency can be used as
```kotlin
implementation("hu.tamasknizner.cookbook:cookbook-api:1.0-SNAPSHOT")
```