import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.openapitools.generator.gradle.plugin.tasks.GenerateTask
import org.openapitools.generator.gradle.plugin.tasks.ValidateTask

plugins {
    val kotlinVersion = "1.8.0"
    kotlin("jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("org.openapi.generator") version "6.3.0"
    id("maven-publish")
    id("org.springframework.boot") version "3.0.4"
    id("io.spring.dependency-management") version "1.1.0"
}

group = "hu.tamasknizner.cookbook"
version = "${System.getenv("CI_COMMIT_TAG") ?: "1.0"}-SNAPSHOT"

kotlin {
    jvmToolchain(17)
}

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.0.4")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("jakarta.validation:jakarta.validation-api")
    implementation("jakarta.annotation:jakarta.annotation-api:2.1.1")
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        val gitlabUrl = System.getenv("CI_API_V4_URL")
        val gitlabProjectId = System.getenv("CI_PROJECT_ID")
        mavenLocal()
        maven {
            url = uri("$gitlabUrl/projects/$gitlabProjectId/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

tasks {
    withType<KotlinCompile> {
        dependsOn("copyGeneratedSources")
        kotlinOptions.jvmTarget = "17"
    }

    withType<ValidateTask> {
        inputSpec.set("$rootDir/definitions/resource.openapi.yaml")
    }

    withType<GenerateTask> {
        generatorName.set("kotlin-spring")
        inputSpec.set("$rootDir/definitions/resource.openapi.yaml")
        outputDir.set("$rootDir/generated")
        apiPackage.set("hu.tamasknizner.cookbook.api")
        modelPackage.set("hu.tamasknizner.cookbook.model")
        configOptions.set(
            mapOf(
                "groupId" to "hu.tamasknizner.cookbook",
                "artifactId" to "api",
                "useSpringBoot3" to "true",
                "useBeanValidation" to "true",
                "interfaceOnly" to "true",
                "useTags" to "true",
            )
        )
        globalProperties.set(
            mapOf(
                "modelDocs" to "false"
            )
        )
    }

    register("copyGeneratedSources", Copy::class) {
        dependsOn("openApiGenerate")
        from("$rootDir/generated/src")
        into("$rootDir/src")
    }

    bootJar {
        enabled = false
    }
}