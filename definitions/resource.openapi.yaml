openapi: 3.0.3
info:
  title: Cookbook API
  description: Cookbook API
  version: 1.0.0
servers:
  - url: 'http://localhost:8080'
    description: Local server
tags:
  - name: CookBook
    description: CookBook API
paths:
  /api/ingredients:
    get:
      operationId: getIngredients
      description: Get all ingredients
      tags:
        - Ingredient
      responses:
        '200':
          $ref: '#/components/responses/AllIngredientResponseApiModel'
  /api/ingredient:
    post:
      operationId: saveIngredient
      description: Saves new ingredient
      tags:
        - Ingredient
      requestBody:
        $ref: '#/components/requestBodies/NewIngredientRequestApiModel'
      responses:
        '200':
          $ref: '#/components/responses/IngredientResponseApiModel'
  /api/ingredient/{id}:
    get:
      operationId: getIngredientById
      description: Get ingredient by id
      tags:
        - Ingredient
      parameters:
        - name: id
          in: path
          description: Ingredient id
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          $ref: '#/components/responses/IngredientResponseApiModel'
  /api/recipe:
    post:
      operationId: saveRecipe
      description: Saves new recipe
      tags:
        - Recipe
      requestBody:
        $ref: '#/components/requestBodies/NewRecipeRequestApiModel'
      responses:
        '200':
          $ref: '#/components/responses/RecipeResponseApiModel'
  /api/recipe/{id}:
    get:
      operationId: getRecipeById
      description: Get recipe by id
      tags:
        - Recipe
      parameters:
        - name: id
          in: path
          description: Recipe id
          required: true
          schema:
            type: string
            format: uuid
      responses:
        '200':
          $ref: '#/components/responses/RecipeResponseApiModel'
  /api/recipes:
    get:
      operationId: getRecipes
      description: Get all recipes
      tags:
        - Recipe
      responses:
        '200':
          $ref: '#/components/responses/AllRecipesResponseApiModel'
components:
  requestBodies:
    NewIngredientRequestApiModel:
      description: New ingredient request body
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/NewIngredientApiModel'
    NewRecipeRequestApiModel:
      description: New recipe request body
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/NewRecipeApiModel'
  responses:
    IngredientResponseApiModel:
      description: Response for ingredient
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/IngredientApiModel'
    AllIngredientResponseApiModel:
      description: Response for get all ingredients
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/AllIngredientsApiModel'
    RecipeResponseApiModel:
      description: Response for recipe
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/RecipeApiModel'
    AllRecipesResponseApiModel:
      description: Response for get all recipes
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/AllRecipesApiModel'
  schemas:
    NewIngredientApiModel:
      type: object
      required:
        - name
      properties:
        name:
          type: string
        description:
          type: string
    IngredientApiModel:
      type: object
      required:
        - id
        - name
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
        description:
          type: string
    AllIngredientsApiModel:
      type: object
      required:
        - ingredients
      properties:
        ingredients:
          type: array
          items:
            $ref: '#/components/schemas/IngredientApiModel'
    AllRecipesApiModel:
      type: object
      required:
        - recipes
      properties:
        recipes:
          type: array
          items:
            $ref: '#/components/schemas/RecipeApiModel'
    RecipeIngredientApiModel:
      type: object
      required:
        - ingredient
        - quantity
      properties:
        ingredient:
          $ref: '#/components/schemas/IngredientApiModel'
        quantity:
          type: string
    RecipeApiModel:
      type: object
      required:
        - id
        - name
        - ingredients
      properties:
        id:
          type: string
          format: uuid
        name:
          type: string
        description:
          type: string
        ingredients:
          type: array
          items:
            $ref: '#/components/schemas/RecipeIngredientApiModel'
    NewRecipeApiModel:
      type: object
      required:
        - name
        - ingredients
      properties:
        name:
          type: string
        description:
          type: string
        ingredients:
          type: array
          items:
            $ref: '#/components/schemas/NewRecipeIngredientApiModel'
    NewRecipeIngredientApiModel:
      type: object
      required:
        - ingredientId
        - quantity
      properties:
        ingredientId:
          type: string
          format: uuid
        quantity:
          type: integer
